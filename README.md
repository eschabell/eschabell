# eschabell

<a rel="me" href="https://fosstodon.org/@ericschabell">Eric D. Schabell verification</a>

You can explore all my <a href="https://gitlab.com/eschabell" target="_blank">coding work on GitLab.</a>
